package com.ttorange.coronavirus;

import android.app.Application;

import com.loopj.android.http.AsyncHttpClient;

public class App extends Application {
    public static String sliderapiurl="http://nationalit.ir/covidapi/";
    public static String apiurl="https://api.covid19api.com/";
    private static AsyncHttpClient client;

    public static AsyncHttpClient getClient(){
        if (client==null){
            client = new AsyncHttpClient();
            client.setConnectTimeout(30);
        }
        return client;
    }
}
