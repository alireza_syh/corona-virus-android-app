package com.ttorange.coronavirus.Adaptors;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ttorange.coronavirus.Models.Slider;
import com.ttorange.coronavirus.R;

import static androidx.core.content.ContextCompat.startActivity;

public class SliderAdaptor extends RecyclerView.Adapter<SliderAdaptor.Holder> {

    Slider[] items;

    public SliderAdaptor(Slider[] items) {
        this.items = items;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemview = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.slide,viewGroup,false);

        return new Holder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int positon) {
        Slider item = items[positon];
        holder.bindData(item);
    }

    @Override
    public int getItemCount() {
        return items.length;
    }

    static class Holder extends RecyclerView.ViewHolder{

        ImageView image;


        public Holder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.roundedImageView);

        }

        public void bindData(final Slider item){

            Glide.with(itemView).load(item.getImglink()).into(image);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent a=new Intent(Intent.ACTION_VIEW, Uri.parse(item.getWeblink()));
                    startActivity(image.getContext(),a,new Bundle());
                }
            });


        }
    }
}
