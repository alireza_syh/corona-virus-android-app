package com.ttorange.coronavirus.Adaptors;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ttorange.coronavirus.Models.Country;
import com.ttorange.coronavirus.Models.Main;
import com.ttorange.coronavirus.Models.Slider;
import com.ttorange.coronavirus.R;

import java.util.List;

import static androidx.core.content.ContextCompat.startActivity;

public class CountryAdaptor extends RecyclerView.Adapter<CountryAdaptor.Holder> {

    List<Country> items;
    Context context;
    public CountryAdaptor(List<Country> items,Context context) {
        this.items = items;
        this.context=context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemview = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.country,viewGroup,false);

        return new Holder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int positon) {
        if (items.get(positon).getSlug().equals("iran")){
            PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("iran", positon).apply();
        }
        Country item = items.get(positon);
        holder.bindData(item);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class Holder extends RecyclerView.ViewHolder{

        TextView country,newd,newc;


        public Holder(@NonNull View itemView) {
            super(itemView);
            country=itemView.findViewById(R.id.countrynamee);
            newd=itemView.findViewById(R.id.cnewdeath);
            newc=itemView.findViewById(R.id.cnewconfrimed);

        }

        public void bindData(Country item){
        country.setText(item.getCountry()+"");
        newd.setText(item.getNewDeaths()+"");
        newc.setText(item.getNewConfirmed()+"");


        }
    }
}
