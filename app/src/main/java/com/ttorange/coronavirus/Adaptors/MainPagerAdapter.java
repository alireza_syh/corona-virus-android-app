package com.ttorange.coronavirus.Adaptors;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ttorange.coronavirus.viewPager.DeathFragment;
import com.ttorange.coronavirus.viewPager.RecoverdFragment;


public class MainPagerAdapter extends FragmentPagerAdapter {

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    int count = 2;

    @Override
    public Fragment getItem(int i) {
        if (i==0)
            return new DeathFragment();
        else
            return new RecoverdFragment();

    }

    @Override
    public int getCount() {
        return count;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String ps="";
        switch (position){
            case 0:
                ps="جانباختگان";
                break;

            case 1:
                ps="بهبود یافتگان";
                break;

        }
        return ps;
    }
}
