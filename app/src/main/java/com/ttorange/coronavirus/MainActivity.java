package com.ttorange.coronavirus;

import android.os.Bundle;
import android.preference.PreferenceManager;


import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.ttorange.coronavirus.Models.Country;
import com.ttorange.coronavirus.Models.Main;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getCountries();
        

    }

    public void getIran(List<Country> iran){
        int position=0;
        for (Country c:
             iran) {
            if (c.getSlug().equals("iran")){
                    PreferenceManager.getDefaultSharedPreferences(this).edit().putInt("iran", position).apply();
            }
            position++;

        }
    }

    private void getCountries(){
        String url= App.apiurl+"summary";

        App.getClient().get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Main item=new Gson().fromJson(response.toString(), Main.class);
                getIran(item.getCountries());

            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}