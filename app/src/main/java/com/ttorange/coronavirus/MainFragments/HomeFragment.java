package com.ttorange.coronavirus.MainFragments;

import android.os.Bundle;
import android.preference.PreferenceManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ttorange.coronavirus.Adaptors.CountryAdaptor;
import com.ttorange.coronavirus.Adaptors.MainPagerAdapter;
import com.ttorange.coronavirus.Adaptors.SliderAdaptor;
import com.ttorange.coronavirus.App;
import com.ttorange.coronavirus.Models.Country;
import com.ttorange.coronavirus.Models.Global;
import com.ttorange.coronavirus.Models.Main;
import com.ttorange.coronavirus.Models.Slider;
import com.ttorange.coronavirus.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView sliderrv,countryrv;
    TabLayout tabhome;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home2, container, false);
        sliderrv=view.findViewById(R.id.sliderrv);
        countryrv=view.findViewById(R.id.countrylist);
        tabhome=view.findViewById(R.id.TabHome);
        ViewPager vphome = view.findViewById(R.id.vphome);
        MainPagerAdapter adapter = new MainPagerAdapter(getChildFragmentManager());
        vphome.setAdapter(adapter);
        tabhome.setupWithViewPager(vphome);

        getsliders();
        getCountries();
        return view;
    }
    private void setCountries(List<Country> items){
        countryrv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));
        CountryAdaptor adp=new CountryAdaptor(items,getContext());
        countryrv.setAdapter(adp);
    }


    private void getCountries(){
        String url= App.apiurl+"summary";

        App.getClient().get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Main item=new Gson().fromJson(response.toString(), Main.class);
                setCountries(item.getCountries());

            }

        });
    }

    private void setSliders(Slider[] items){
        sliderrv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));
        SliderAdaptor adp=new SliderAdaptor(items);
        sliderrv.setAdapter(adp);
    }

    private void getsliders(){
        String url= App.sliderapiurl;

        App.getClient().get(url ,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                Slider[] item=new Gson().fromJson(response.toString(), Slider[].class);
                setSliders(item);
            }
        });
    }
}