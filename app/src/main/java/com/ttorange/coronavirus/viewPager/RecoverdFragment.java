package com.ttorange.coronavirus.viewPager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.ttorange.coronavirus.App;
import com.ttorange.coronavirus.Models.Country;
import com.ttorange.coronavirus.Models.Global;
import com.ttorange.coronavirus.Models.Main;
import com.ttorange.coronavirus.R;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RecoverdFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecoverdFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    TextView NewRecovered,TotalRecovered,IranNewRecovered,IranTotalRecovered;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public RecoverdFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RecoverdFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecoverdFragment newInstance(String param1, String param2) {
        RecoverdFragment fragment = new RecoverdFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_recoverd, container, false);
        NewRecovered=view.findViewById(R.id.homenewrecoverd);
        TotalRecovered=view.findViewById(R.id.hometotalrecieved);
        IranNewRecovered=view.findViewById(R.id.irannewrecoverd);
        IranTotalRecovered=view.findViewById(R.id.irantotalrecieved);
        getCountries();
        return view;
    }

    private void setTextviews(Global item){

        NewRecovered.setText(item.getNewRecovered()+"");
        TotalRecovered.setText(item.getTotalRecovered()+"");
    }

    private void setirantv(Country iran){
        IranNewRecovered.setText(iran.getNewRecovered()+"");
        IranTotalRecovered.setText(iran.getTotalRecovered()+"");

    }

    private void getCountries(){
        String url= App.apiurl+"summary";

        App.getClient().get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Main item=new Gson().fromJson(response.toString(), Main.class);
                setTextviews(item.getGlobal());
                int position = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt("iran",0);
                setirantv(item.getCountries().get(position));

            }

        });
    }
}